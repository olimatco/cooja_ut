/**
 * \file
 *         bbs-setboard.h - select ContikiBBS message boards - header file
 *
 * \author
 *         (c) 2009-2011 by Niels Haedecke <n.haedecke@unitybox.de>
 */

#ifndef __BBS_SETBOARD_H__
#define __BBS_SETBOARD_H__

#include "shell.h"
#include "sys/log.h"
#include "bbsdefs.h"

void bbs_setboard_init(void);

#endif /* __BBS_SETBOARD_H__ */
