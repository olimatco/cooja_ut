/**
 * \file
 *         bbs-page.h - page SysOp from ContikiBBS - header file
 * \author
 *         (c) 2011 by Niels Haedecke <n.haedecke@unitybox.de>
 */


#ifndef __BBS_PAGE_H__
#define __BBS_PAGE_H__

#include "shell.h"
#include "sys/log.h"
#include "bbsdefs.h"

void bbs_page_init(void);

#endif /* __BBS_PAGE_H__ */
