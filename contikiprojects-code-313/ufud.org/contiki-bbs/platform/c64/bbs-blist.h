/**
 * \file
 *         bbs-blist.h - show ContikiBBS message boards - header file
 * \author
 *         (c) 2011 by Niels Haedecke <n.haedecke@unitybox.de>
 */

#ifndef __BBS_BOARD_H__
#define __BBS_BOARD_H__

#include "shell.h"
#include "sys/log.h"
#include "bbsdefs.h"
#include "bbs-file.h"

void bbs_blist_init(void);

#endif /* __BBS_BOARD_H__ */
