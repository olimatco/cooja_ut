#include <stdio.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

int nodeID;
int outputPowerLevel = 3;

int allowTransmit = 0;

int nbrMsgs = 0;

// #blindNodes * 6[slots] + 1[for senderID]
#define TABLE_SLOTS 43

// nodes_table[0] = senderID
// Node_n => | sndr1_ID | RSSI | sndr2_ID | RSSI | sndr3_ID | RSSI |
int nodes_table[TABLE_SLOTS];

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "Z1-based Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	nodeID = linkaddr_node_addr.u8[0];

	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	while (1) {

		cc2420_set_txpower(outputPowerLevel);
		
		/* Delay 2-4 seconds */
		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
		//etimer_set(&et, 0);

		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		if (allowTransmit > 0){

			//set senderID
			nodes_table[0] = nodeID;
		
			packetbuf_copyfrom(nodes_table, (int) sizeof(nodes_table));

//			int a[5] = {10,20,30,-40,0};
//
//			packetbuf_copyfrom(a, (int) sizeof(a));
	
			broadcast_send(&broadcast);

			printf("transmitting from %d\n", nodeID);
		}

		//else{
			// nothing
		//}
	}

	PROCESS_END();
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {

	//if(nbrMsgs > 12){

		packetbuf_attr_t rssi;
		rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);

		int (*msg)[TABLE_SLOTS] = (int (*)[TABLE_SLOTS]) packetbuf_dataptr();

		int senderID = (int)((int *) msg)[0]/256;

		printf("[%d] - senderID: [%d] .. RSSI: [%d]\n", nbrMsgs, (int) senderID, (int) rssi);
	//}

	nbrMsgs++;

/*
	packetbuf_attr_t rssi;
	rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);

	int (*msg)[TABLE_SLOTS] = (int (*)[TABLE_SLOTS]) packetbuf_dataptr();

//	int (*msg)[5] = (int (*)[5]) packetbuf_dataptr();

	int senderID = (int)((int *) msg)[0]/256;

//	printf("senderID: %d\n", senderID);

	if(nbrMsgs < 3){

		//-- insert values of this node..
		//------------------------------------------------------------------
		
		int index = 6 * (nodeID - 6) + 1;
	
//		printf("index: %d\n", index);

		// insert value in next empty slot

		int i;

		for(i=0;i<6;i+=2){

			if(nodes_table[index+i] == 0){
				nodes_table[index+i] = (int) senderID;
				nodes_table[index+i+1] = (int) rssi;

				nbrMsgs++;

				printf("%d from %d\n", nbrMsgs, senderID);

				//----------------
				// print array

				int i;
		
				printf("\n\n");
		
				for(i=1;i<TABLE_SLOTS;i++){
		
					int value = (int)((int *) msg)[i];
				
					if(value==255){
						value = 0;
					}

					else{
						value = value/256;
					}

					printf("%d\t", value);
		
					if(i%6==0){
						printf("\n");
					}
				}

				printf("\n\n");
				
				break;
			}
			
			else{
				if(nodes_table[index+i] == senderID){
					// ignore message from same sender
					break;
				}
				
				else{
					//do nothing
				}
			}

		}

		//-----------------------------------------------

		//-- copy values from received message
		//------------------------------------

		index = 6 * (senderID - 6) + 1;
		
		for(i=0;i<6;i++){
			nodes_table[index+i] = ((int *) msg)[index+i];
		}

		//------------------------------------

		if(nbrMsgs > 2){
			allowTransmit = 1;
			printf("allowTransmit\n");
		}

	}

	else{
		//printf("already rcvd 3 msgs\n");
	}

/*
	int i;

	printf("\n\n");

	for(i=0;i<5;i++){

		int value = (int)((int *) msg)[i];
		
		if(value==255){
			value = 0;
		}

		else{
			value = value/256;
		}

		printf("%d\t", value);
	}

	printf("\n\n");
*/
//}
}
