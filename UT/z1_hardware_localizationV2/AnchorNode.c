#include <stdio.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

int nodeID;
int outputPowerLevel = 3;

// #blindNodes * 6[slots] + 1[for senderID]
#define TABLE_SLOTS 43

// nodes_table[0] = senderID
// Node_n => | sndr1_ID | RSSI | sndr2_ID | RSSI | sndr3_ID | RSSI |
int nodes_table[TABLE_SLOTS];

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "Z1-based Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	nodeID = linkaddr_node_addr.u8[0];

	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	//delay for some seconds
	//etimer_set(&et, 9000);
	//PROCESS_WAIT_EVENT_UNTIL(&et);

	while (1) {

		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
		
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		cc2420_set_txpower(outputPowerLevel);
		
		//set senderID
		nodes_table[0] = nodeID;
		
		packetbuf_copyfrom(nodes_table, sizeof(nodes_table));
	
		broadcast_send(&broadcast);

		printf("\n<OUT:(%d)>\n", nodeID);
	}

	PROCESS_END();
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {
	// do nothing
}
