/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * Original file:	Testing the broadcast layer in Rime
 * author:			Adam Dunkels <adam@sics.se>
 *
 * dLoc Localization was implemented inside the original file by:
 *
 * Hussein Al-Olimat (Hussein.AlOlimat@msn.com)
 *
 * and
 *
 * Ahmad Javaid (yazdan@ieee.org)
 *
 * The University of Toledo
 * Spring 2014
 *
 * Works with the following motes:
 * 1- Exp5438	20-bit
 * 2- Z1 		20-bit
 * 3- MicaZ		?
 * 4- Sky		16-bit
 *
 */

#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

//#include "serial-shell.h"
//#include "shell-ps.h"
//#include "shell-file.h"
#include "shell-text.h"

#include "stdlib.h"
#include "stdio.h"
#include "math.h"

#include "apps/powertrace/powertrace.h"

#include "recieved_anchor_info.h"
#include "nodes_positions.h"

#define MAX_ANCHOR_NODES 16
static struct anchor_info rec_anchors_info[MAX_ANCHOR_NODES];

int nodeID = 0;

int isLocalized = 0;
int stopBroadcasting = 0;
int helpMsgsSent = 0;

float estX = 0;
float estY = 0;

// actual x and y
float myX = 0;
float myY = 0;

//update myX and myY
void getMyPosition();

//prints the location found by Trilateration Linear Formulation
void printEstimatedLocation();
int getFractionFromNbr(float number);

void triLat();

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "dLoc Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	nodeID = linkaddr_node_addr.u8[0];

	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	// TODO:	Use Powertrace application to trace the power
	//			consumption of the network during localization
	//			<before> while loop: powertrace_sniff(POWERTRACE_ON);
	//			<inside> while loop: powertrace_print("#P");

	while (1) {

		cc2420_set_txpower(31);
		
		/* Delay 2-4 seconds */
		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
		//etimer_set(&et, 0);

		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		// if the node is already localized by triLat and did not
		// broadcast its location yet enter here
		if (isLocalized > 0 && stopBroadcasting < 1) {

			// msg => {localized?, id, x, y}
			// localized? = 1 ==>>> localized
			float msg[4] = { isLocalized, nodeID, estX, estY };

			packetbuf_copyfrom(msg, (int) sizeof(msg));
			broadcast_send(&broadcast);

			printf("\n<OUT:(%d)> Localized itself, Broadcasting to help others!\n", nodeID);

			//outputs the new location in the log
			printEstimatedLocation();

			// TODO:	To improve dLoc allow the already localized nodes to broadcast the localization table
			// 			once they receive new information. This can help side nodes to localize themselves using
			// 			triangulation to far away nodes.

			stopBroadcasting = 1;
		}

		else {

			// if I am allowed to broadcast enter. this is the normal first broadcast
			// of the blind node asking for help from anchor nodes
			if (stopBroadcasting < 1) {

				startDLoc();

				stopBroadcasting = 1;

				printf("\n<OUT:(%d)> RunDLoc!\n", nodeID);
			}

			else {
				//printf("\n<-:(%d)> Standby! ... Localized? : %d\n", nodeID, isLocalized);
			}
		}
	}

PROCESS_END();
}

//=============

//= Receive ===

//=============

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {
	printf("Broadcast received\n");
}

void startDLoc(){
	rec_anchors_info[0].id = 53;
	rec_anchors_info[0].x = 171.48;
	rec_anchors_info[0].y = 129.7;
	rec_anchors_info[0].rssi = -10;

	rec_anchors_info[0].id = 61;
	rec_anchors_info[0].x = 164.67;
	rec_anchors_info[0].y = 149.88;
	rec_anchors_info[0].rssi = 27;

	rec_anchors_info[0].id = 74;
	rec_anchors_info[0].x = 182.16;
	rec_anchors_info[0].y = 143.42;
	rec_anchors_info[0].rssi = -3;

	triLat();
	isLocalized = 1;
	stopBroadcasting = 0;
}

void triLat(){

	// TODO: this should be received from the source..
	int pMax = 31;

	float n0X = rec_anchors_info[0].x;
	float n0Y = rec_anchors_info[0].y;
	float n1X = rec_anchors_info[1].x;
	float n1Y = rec_anchors_info[1].y;
	float n2X = rec_anchors_info[2].x;
	float n2Y = rec_anchors_info[2].y;

	int n0_RSSI = rec_anchors_info[0].rssi;
	int n1_RSSI = rec_anchors_info[1].rssi;
	int n2_RSSI = rec_anchors_info[2].rssi;

	/*
	// =======================================================================================================
	// = dLoc math ===========================================================================================
	// =======================================================================================================

	// XXX: Src => Localization in Wireless Sensor Networks by Francisco Santos .. dLoc reference
	double q = sqrt(pow((n0X - n2X),2) + pow((n0Y - n2Y),2));
	double r = sqrt(pow((n0X - n1X),2) + pow((n0Y - n1Y),2));

	// XXX: Our RSSI equation
	double t = (-pMax/ 52.8) * (n0_RSSI - 35.5);
	double v = (-pMax/ 52.8) * (n1_RSSI - 35.5);
	double u = (-pMax/ 52.8) * (n2_RSSI - 35.5);

	//Trilateration -----------------------------------

	estX = (1/(2*q)) * (pow(q,2) + pow(t,2) - pow(u,2));

	if(n2Y == 0){
		estY = (q * (pow(r,2) + pow(t,2) - pow(v,2)) - n2X * (pow(q,2) + pow(t,2) - pow(u,2)))/(2*q*(n2Y+1));
	}
	else{
		estY = (q * (pow(r,2) + pow(t,2) - pow(v,2)) - n2X * (pow(q,2) + pow(t,2) - pow(u,2)))/(2*q*n2Y);
	}
	// =======================================================================================================
	*/

	// XXX: Our RSSI equation
	float t = (-pMax/ 52.8) * (n0_RSSI - 35.5);
	float u = (-pMax/ 52.8) * (n1_RSSI - 35.5);
	float v = (-pMax/ 52.8) * (n2_RSSI - 35.5);

	// Math here is from the 4 equations in dLoc paper, not original dLoc math with matrices.

	// _________________1st Approach (some random seeds cause int overflow in the decimal points)_____________
	/*

	 float va_2_1 = (n1X/10)*(n1X/10);
	float va_2_2 = (n2X/10)*(n2X/10);

	float va_3_1 = (n1Y/10)*(n1Y/10);
	float va_3_2 = (n2Y/10)*(n2Y/10);

	float va_1 = (u*u) - (v*v);
	float va_2 = va_2_1 - va_2_2;
	float va_3 = va_3_1 - va_3_2;

	float va_p1 = (va_1/100) - va_2;
	float va = va_p1 - va_3;

	//this value is multiplied by 100 later
	va = va/2;

	///--------------------------------------- vb
	float vb_2_1 = (n1X/10)*(n1X/10);
	float vb_2_2 = (n0X/10)*(n0X/10);

	float vb_3_1 = (n1Y/10)*(n1Y/10);
	float vb_3_2 = (n0Y/10)*(n0Y/10);

	float vb_1 = (u*u) - (t*t);
	float vb_2 = vb_2_1 - vb_2_2;
	float vb_3 = vb_3_1 - vb_3_2;

	float vb_p1 = (vb_1/100) - vb_2;
	float vb = vb_p1 - vb_3;

	//this value is multiplied by 100 later
	vb = vb/2;

	//--------------------------------------
	float y_n = ((vb * 100)*(n2X-n1X) - (va * 100)*(n0X-n1X)) ;
	float y_d = ((n0Y -n1Y)*(n2X-n1X) - (n2Y - n1Y)*(n0X-n1X));

	estY = y_n/y_d;
	//--------------------------------------

	float x_n = (va * 100) - estY * (n2Y-n1Y);
	float x_d = n2X - n1X;

	estX = x_n/x_d;

	 */
	// _________________/1st Approach_________________________________________________________________________

	// _________________2nd Approach__________________________________________________________________________

	int n1X_int = (int) n1X * 10;

	float va_2_1 = ((float)((int)(n1X*10))/100)*((float)((int)(n1X*10))/100);
	float va_2_2 = ((float)((int)(n2X*10))/100)*((float)((int)(n2X*10))/100);

	float va_3_1 = ((float)((int)(n1Y*10))/100)*((float)((int)(n1Y*10))/100);
	float va_3_2 = ((float)((int)(n2Y*10))/100)*((float)((int)(n2Y*10))/100);

	float va_1 = ((float)((int)(u*10))/10)*((float)((int)(u*10))/10)
				- ((float)((int)(v*10))/10)*((float)((int)(v*10))/10);

	float va_2 = va_2_1 - va_2_2;
	float va_3 = va_3_1 - va_3_2;

	float va_p1 = (va_1/100) - va_2;
	float va = va_p1 - va_3;

	//this value is multiplied by 100 later
	va = va/2;

	///--------------------------------------- vb
	float vb_2_2 = ((float)((int)(n0X*10))/100)*((float)((int)(n0X*10))/100);
	float vb_3_2 = ((float)((int)(n0Y*10))/100)*((float)((int)(n0Y*10))/100);

	float vb_1 = ((float)((int)(u*10))/10)*((float)((int)(u*10))/10)
					- ((float)((int)(t*10))/10)*((float)((int)(t*10))/10);

	float vb_2 = va_2_1 - vb_2_2;
	float vb_3 = va_3_1 - vb_3_2;

	float vb_p1 = (vb_1/100) - vb_2;
	float vb = vb_p1 - vb_3;

	vb = vb/2;

	//--------------------------------------
	float y_n = ((vb * 100)*(n2X-n1X) - (va * 100)*(n0X-n1X)) ;
	float y_d = ((n0Y -n1Y)*(n2X-n1X) - (n2Y - n1Y)*(n0X-n1X));

	estY = y_n/y_d;
	//--------------------------------------

	float x_n = (va * 100) - estY * (n2Y-n1Y);
	float x_d = n2X - n1X;

	estX = x_n/x_d;

	// _____________________________/2nd Approach_____________________________________________________________

	printf("(%d) estimated X => %d estimated Y => %d", nodeID, (int) estX, (int) estY);
}

void getMyPosition(){
	myX = positions_100Nodes[nodeID-1][0];
    myY = positions_100Nodes[nodeID-1][1];
}

int getFractionFromNbr(float number){

	int realNbr = (int) number;
	int fraction = (int) ((number - realNbr)*100);

	return fraction;
}

//-------------------------------------------------------------------------------
// This function is doing magic, Don't ever try to do this with real compilers :S
//-------------------------------------------------------------------------------
// MSP430 does not support float printing and this function is doing that for us
// The function simply takes a float like 3.04 and divide it into two parts, the
// first part is the real number and the second is the fraction. Then if the
// fraction is less than 10 it means we need to put a zero after the decimal
// point or the number will be larger than the original number.
//-------------------------------------------------------------------------------
// Also, non of sprintf or itoa worked. For some reason they cause logical errors
// ------------------------------------------------------------------------------

void printEstimatedLocation(){

	getMyPosition();

	//------------------------
	int actX_real = (int) myX;
	int actX_frac = (int) getFractionFromNbr(myX);

	int actY_real = (int) myY;
	int actY_frac = (int) getFractionFromNbr(myY);

	int estX_real = (int) estX;
	int estX_frac = (int) getFractionFromNbr(estX);

	int estY_real = (int) estY;
	int estY_frac = (int) getFractionFromNbr(estY);

	//------------------------

	char* actX_frac_AdditonaldecimalPoint = "";
	char* actY_frac_AdditonaldecimalPoint = "";
	char* estX_frac_AdditonaldecimalPoint = "";
	char* estY_frac_AdditonaldecimalPoint = "";

	//------------------------

	if(actX_frac < 10){
		actX_frac_AdditonaldecimalPoint = "0";

		if(actX_frac < 0){
			actX_frac *= -1;
		}
	}

	if(actY_frac < 10){
		actY_frac_AdditonaldecimalPoint = "0";

		if(actY_frac < 0){
			actY_frac *= -1;
		}
	}

	if(estX_frac < 10){
		estX_frac_AdditonaldecimalPoint = "0";

		if(estX_frac < 0){
			estX_frac *= -1;
		}
	}

	if(estY_frac < 10){
		estY_frac_AdditonaldecimalPoint = "0";

		if(estY_frac < 0){
			estY_frac *= -1;
		}
	}

	printf("\n<-:(%d)> Actual-XY: [%d.%s%d:%d.%s%d]  +  dLoc-XY: [%d.%s%d:%d.%s%d]\n\n",
			nodeID,
			actX_real, actX_frac_AdditonaldecimalPoint, actX_frac,
			actY_real, actY_frac_AdditonaldecimalPoint, actY_frac,
			estX_real, estX_frac_AdditonaldecimalPoint, estX_frac,
			estY_real, estY_frac_AdditonaldecimalPoint, estY_frac
	);
}
