/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * Original file:	Testing the broadcast layer in Rime
 * author:			Adam Dunkels <adam@sics.se>
 *
 * dLoc Localization was implemented inside the original file by:
 *
 * Hussein Al-Olimat (Hussein.AlOlimat@msn.com)
 *
 * and
 *
 * Ahmad Javaid (yazdan@ieee.org)
 *
 * The University of Toledo
 * Spring 2014
 *
 */

#include <stdio.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

#include "recieved_anchor_info.h"
#include "nodes_positions.h"

int nodeID = 0;

//read x,y coordinates of anchors
//simulating a GPS chip
void getMyPosition();

//updated by getMyPosition
float myX = 0;
float myY = 0;

int allowBroadcast = 0;
int alreadyBroadcasted = 0;

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "dLoc Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	nodeID = linkaddr_node_addr.u8[0];
	getMyPosition();

	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	while (1) {

		/* Delay 2-4 seconds */
		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
		//etimer_set(&et, 0);

		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		if (allowBroadcast > 0 && alreadyBroadcasted < 1) {

			// anchor nodes always know their positions
			int isLocalized = 1;

			float msg[4] = { isLocalized, nodeID, myX, myY};

			// TODO: 	To improve dLoc a method can be used here to dynamically change the transmission power
			//			this can change the transmission power of the Msg
			//			==> cc2420_set_txpower(15);

			cc2420_set_txpower(31);

			packetbuf_copyfrom(msg, (int) sizeof(msg));
			broadcast_send(&broadcast);

			// TODO:	To improve dLoc allow the already localized nodes to broadcast the localization table
			// 			once they receive new information. This can help side nodes to localize themselves using
			// 			triangulation to far away nodes.

			allowBroadcast = 0;

			alreadyBroadcasted = 1;

			printf("\n<OUT:(%d)> Broadcasting to help others!\n", nodeID);
		}

		else{
			//printf("\n<-:(%d)> Standby!\n", nodeID);
		}

		//printf("\n<-:(%d)> LOOPED!\n", nodeID);
	}

	PROCESS_END();
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {

	float (*msg)[4] = (float (*)[4]) packetbuf_dataptr();

	// if the message is from a non localized node allow to broadcast
	if ((int)((float *) msg)[0] < 1) {
		allowBroadcast = 1;
		printf("\n<IN:(%d)> Message from a non-localized node (ID:%d) requesting help!\n", nodeID, (int)((float *) msg)[1]);
	}

	// else do nothing
	else{
		printf("\n<IN:(%d)> Message ignored non-localization request from node (ID:%d)!\n", nodeID, (int)((float *) msg)[1]);
	}
}

void getMyPosition(){
	myX = positions_100Nodes[nodeID-1][0];
    myY = positions_100Nodes[nodeID-1][1];
}
