#include <stdio.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

int nodeID;

int nbrMsgs = 0;

// #blindNodes * 6[slots] + 1[for senderID]
#define TABLE_SLOTS 43

// nodes_table[0] = senderID
// Node_n => | sndr1_ID | RSSI | sndr2_ID | RSSI | sndr3_ID | RSSI |
int nodes_table[TABLE_SLOTS];

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "Z1-based Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	nodeID = linkaddr_node_addr.u8[0];

	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	while (1) {

		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
		
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

		//never send anything
	}

	PROCESS_END();
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {

	packetbuf_attr_t rssi;
	rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);

	int (*msg)[TABLE_SLOTS] = (int (*)[TABLE_SLOTS]) packetbuf_dataptr();

	nbrMsgs++;
	printf("\nmsg#: %d\n", nbrMsgs);
	
	int senderID = (int)((int *) msg)[0]/256;

	printf("senderID: %d\n", senderID);

	//----------------
	// print array

	int i;
		
	printf("\n\n");
		
	for(i=1;i<TABLE_SLOTS;i++){
		int value = (int)((int *) msg)[i];
				
		if(value==255){
			value = 0;
		}

		else{
			value = value/256;
		}

		printf("%d\t", value);
	
		if(i%6==0){
			printf("\n");
		}
	}

}

