/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * Original file:	Testing the broadcast layer in Rime
 * author:			Adam Dunkels <adam@sics.se>
 *
 * dLoc Localization was implemented inside the original file by:
 *
 * Hussein Al-Olimat (Hussein.AlOlimat@msn.com)
 *
 * and
 *
 * Ahmad Javaid (yazdan@ieee.org)
 *
 * The University of Toledo
 * Spring 2014
 *
 */

#include <stdio.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "dev/button-sensor.h"
#include "sys/node-id.h"
#include "dev/leds.h"

#include "serial-shell.h"
#include "shell-ps.h"
#include "shell-file.h"
#include "shell-text.h"

int nodeID = 0;

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "dLoc Localization Process");
AUTOSTART_PROCESSES(&broadcast_process);
//----
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from);
//----
static const struct broadcast_callbacks broadcast_call = { broadcast_recv };
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(broadcast_process, ev, data) {

	serial_shell_init();
		shell_ps_init();
		shell_file_init();  // for printing out files
		shell_text_init();  // for binprint
	
	nodeID = linkaddr_node_addr.u8[0];
	
	static struct etimer et;

	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);

	while (1) {

		/* Delay 2-4 seconds */
		etimer_set(&et, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
	}

	PROCESS_END();
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from) {

	int16_t (*msg)[4] = (int16_t (*)[4]) packetbuf_dataptr();

	printf("\nNew message from a node ID => (%u)\n", ((int16_t *) msg)[1]);
}
